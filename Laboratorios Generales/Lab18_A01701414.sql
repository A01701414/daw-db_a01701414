


/*1.-*/ SELECT COUNT(E.Clave) AS 'Cantidades',SUM(E.Cantidad*(M.Costo + M.PorcentajeImpuesto)) as 'Importe Total' FROM Materiales M, Entregan E WHERE M.Clave = E.Clave AND E.Fecha BETWEEN '01/01/1997' AND '31/12/1997';

/*2.-*/ SELECT P.RazonSocial,COUNT(E.RFC) AS 'Entregas',SUM(E.Cantidad*(M.Costo + M.PorcentajeImpuesto)) as 'Importe Total' FROM Materiales M, Entregan E,Proveedores P WHERE M.Clave = E.Clave AND P.RFC = E.RFC GROUP BY E.RFC, RazonSocial;

/*3.-*/ SELECT M. Clave,M.Descripcion,SUM(E.Cantidad)as Total ,Min(E.Cantidad) as Minimo ,Max(E.Cantidad) as Maximo, SUM(E.Cantidad*(M.Costo + M.PorcentajeImpuesto)) as Importe_Total FROM  Entregan E, Materiales M Where E.Clave=M.Clave GROUP BY m.Clave,m.Descripcion HAVING AVG(Cantidad) > 400

/*4.-*/

CREATE view Razon_prom_clave as (
SELECT RazonSocial, AVG(e.Cantidad)AS Promedio_Entregado, E.Clave  FROM Proveedores P,  Entregan E WHERE E.RFC = P.RFC  GROUP BY E.RFC,RazonSocial,E.Clave)

SELECT R.RazonSocial,r.Promedio_Entregado,m.Clave,m.Descripcion FROM Razon_prom_clave R, Materiales M WHERE m.Clave = r.Clave and r.Promedio_Entregado >= 500

/*5.-*/

CREATE view Razon_prom_clave as (
SELECT RazonSocial, AVG(e.Cantidad)AS Promedio_Entregado, E.Clave  FROM Proveedores P,  Entregan E WHERE E.RFC = P.RFC  GROUP BY E.RFC,RazonSocial,E.Clave)

SELECT R.RazonSocial,r.Promedio_Entregado,m.Clave,m.Descripcion FROM Razon_prom_clave R, Materiales M WHERE m.Clave = r.Clave and (r.Promedio_Entregado < 370 or r.Promedio_Entregado >450) ORDER BY  Promedio_Entregado ASC

INSERT INTO Materiales VALUES (1440,'Grava',150,2.88)
INSERT INTO Materiales VALUES (1450,'Gravilla',200,2.9)
INSERT INTO Materiales VALUES (1460,'Tepetate',110,2.92)
INSERT INTO Materiales VALUES (1470,'Arenilla',300,2.94)
INSERT INTO Materiales VALUES (1480,'Madera',400,2.96)

/*6.-*/

CREATE VIEW No_Entregados AS (
  SELECT M.Clave
  FROM Materiales M
  EXCEPT
  SELECT E.Clave
  FROM Entregan E
  GROUP BY E.Clave
)
SELECT N.Clave, M.Descripcion FROM No_Entregados N, Materiales M WHERE M.Clave = N.Clave

/*7.-*/

CREATE VIEW MexQro AS(
SELECT E.RFC FROM Entregan E,Proyectos P WHERE E.Numero = P.Numero AND (P.Denominacion = 'Vamos Mexico' or P.Denominacion = 'Queretaro Limpio') GROUP BY RFC)

Select P.RazonSocial FROM MexQro M , Proveedores P WHERE M.RFC = P.RFC

/*8.-*/

CREATE VIEW MaterialesNoYucatan AS (

SELECT M.Clave FROM Materiales M
    EXCEPT
SELECT E.Clave FROM Entregan E, Proyectos P WHERE  E.Numero = P.Numero AND P.Denominacion = 'CIT Yucatan' GROUP BY E.Clave)


SELECT M.Descripcion FROM MaterialesNoYucatan Y, Materiales M WHERE m.Clave = y.Clave

/*9.-*/

CREATE VIEW Prom_Entrega_Provedor AS (
SELECT E.RFC, AVG(E.Cantidad) as Promedio_Cantidad FROM Entregan E GROUP BY E.RFC)

SELECT P.RazonSocial , EP.Promedio_Cantidad FROM Proveedores P, Prom_Entrega_Provedor EP WHERE P.RFC = EP.RFC AND EP.Promedio_Cantidad >(SELECT Promedio_Cantidad FROM Prom_Entrega_Provedor WHERE RFC ='VAGO780901')


/*10.-*/

CREATE VIEW ParticipantesDur AS (
SELECT E.RFC, e.Cantidad, E.Fecha FROM Entregan E, Proyectos P WHERE E.Numero = P.Numero AND P.Denominacion = 'Infonavit Durango')

CREATE view ParticipantesDur2000 AS (
SELECT RFC, Cantidad FROM ParticipantesDur WHERE Fecha BETWEEN '01/01/2000' AND '31/12/2000')

CREATE view ParticipantesDur2001 AS (
SELECT RFC, Cantidad FROM ParticipantesDur WHERE Fecha BETWEEN '01/01/2001' AND '31/12/2001')

CREATE  VIEW TopParticipantes2000_than_2001 AS (
SELECT P.RFC FROM ParticipantesDur2000 P, ParticipantesDur2001 Ps WHERE P.RFC = Ps.RFC AND P.Cantidad > PS.Cantidad)

SELECT T.RFC,P.RazonSocial FROM TopParticipantes2000_than_2001 T, Proveedores P WHERE P.RFC = T.RFC
