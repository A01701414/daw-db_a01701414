-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 27, 2017 at 08:36 PM
-- Server version: 5.5.57-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `petz`
--
CREATE DATABASE IF NOT EXISTS `petz` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `petz`;

-- --------------------------------------------------------

--
-- Table structure for table `Zombies`
--

DROP TABLE IF EXISTS `Zombies`;
CREATE TABLE IF NOT EXISTS `Zombies` (
  `Id_Zombie` int(3) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(25) NOT NULL,
  `Apellido_P` varchar(25) NOT NULL,
  `Apellido_M` varchar(25) NOT NULL,
  `Estado` varchar(25) NOT NULL,
  `Registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Transicion` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`Id_Zombie`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `Zombies`
--

INSERT INTO `Zombies` (`Id_Zombie`, `Nombre`, `Apellido_P`, `Apellido_M`, `Estado`, `Registro`, `Transicion`) VALUES
(1, 'Roberto', 'Rojas', 'Ramirez', 'Infeccion', '2017-10-27 18:20:19', '2017-10-27 18:20:19'),
(5, 'Rod', 'ROb', 'Re', 'Transformacion', '2017-10-27 18:43:09', '2017-10-27 18:43:09'),
(6, 'Rotito', 'ROL', 'TO', 'Completamente Muerto', '2017-10-27 19:02:35', '2017-10-27 19:02:35'),
(7, 'Hilario', 'RT ', 'Suarez', 'Coma', '2017-10-27 20:27:47', '2017-10-27 20:27:47'),
(8, 'Gilberto', 'Carsoli ', 'Gonzales', 'Coma', '2017-10-27 20:27:56', '2017-10-27 20:27:56'),
(9, 'Rogelio', 'Ramo', 'Garcia', 'Completamente Muerto', '2017-10-27 20:28:06', '2017-10-27 20:28:06'),
(10, 'Quintillo', 'cRISALIDA', 'Hernandez', 'Completamente Muerto', '2017-10-27 20:28:21', '2017-10-27 20:28:21'),
(11, 'Manuel', 'Garcia', 'Huerta', 'Transformacion', '2017-10-27 20:28:32', '2017-10-27 20:28:32'),
(12, 'Ivett', 'Fernandez', 'Fonseca', 'Transformacion', '2017-10-27 20:28:49', '2017-10-27 20:28:49'),
(13, 'Pablo', 'Pradera', 'Ayala', 'Infeccion', '2017-10-27 20:28:58', '2017-10-27 20:28:58');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
