<?php
    
    function conectarBD(){ //Conectar a base de datos
      $nombreServidor = "localhost";
      $usuario = "ischwifty";
      $contrasena = "";
      $nombreBD= "petz";
      //Realiza la conexion
      $conexion = mysqli_connect($nombreServidor,$usuario,$contrasena,$nombreBD);
    
      if(!$conexion){//Si falla devuelve el error
        die("Conexion Fallida ". mysqli_connect_error());
      }
    
      return $conexion;
    }
    
    function cerrarBD($conexion){ //Cerrar Base de Datos
      mysqli_close($conexion);
    }
    
    function registrarZombie($Nombre,$Apellido_P,$Apellido_M,$Estado){//Funcion para registrar usuarios nuevos
    $conexion = conectarBD();//Conecta a la base de datos
    $consulta ='INSERT INTO Zombies(Nombre,Apellido_P,Apellido_M,Estado,Transicion) VALUES (?,?,?,?,CURRENT_TIMESTAMP()) ';//listo
    if(!($sentencia = $conexion->prepare($consulta))){//Prepara al consulta
      die('Preparacion de consulta fallida('.$conexion->errno.') '.$conexion->error);
    }
    if(!$sentencia->bind_param("ssss",$Nombre,$Apellido_P,$Apellido_M,$Estado)){//Agrega parametros a la consulta
      die('Vinculacion de parametros fallida('.$sentencia->errno.') '.$sentencia->error);
    }
    if(!$sentencia->execute()){//Ejecuta la consulta
      die('Ejecucion Fallida ('.$sentencia->errno.') '.$sentecia->error);
    }
    cerrarBD($conexion);//Ciera la bae de datos
    }
    
    function totalZombies(){
        $conexion = conectarBD();
        $consulta = "SELECT COUNT( 'Nombre') FROM Zombies";
        $resultado = mysqli_query($conexion,$consulta);
        $line = mysqli_fetch_array($resultado, MYSQLI_BOTH);
        
        cerrarBD($conexion);//Ciera la bae de datos
        return $line;
        
    }
    function totalEstado($Estado){
        $conexion = conectarBD();
        $consulta = "SELECT COUNT( 'Nombre') FROM Zombies WHERE Estado ='".$Estado."'";
        $resultado = mysqli_query($conexion,$consulta);
         $line = mysqli_fetch_array($resultado, MYSQLI_BOTH);
        
        cerrarBD($conexion);//Ciera la bae de datos
        return $line;
    }
    function noMuertos(){
        $conexion = conectarBD();
        $consulta = "SELECT COUNT(  'Nombre' ) FROM  Zombies WHERE Estado !=  'Completamente Muerto'";
        $resultado = mysqli_query($conexion,$consulta);
        $line = mysqli_fetch_array($resultado, MYSQLI_BOTH);
        
        cerrarBD($conexion);//Ciera la bae de datos
        return $line;
    }
    
    
    
    
    
        function getAccounts(){
        $conexion = conectarBD();
        $sql = "SELECT * FROM Zombies ORDER BY Registro DESC";
        $result = mysqli_query($conexion,$sql);
        if(mysqli_num_rows($result) > 0){
            echo '<table class="responsive-table striped ses" ><tr><th>Nombre</th><th>Apellidos</th><th>Estado</th><th>Registro</th><th>Transicion</th></tr>';
            while($row = mysqli_fetch_assoc($result)){
                echo "<tr>";
                echo "<td>".$row["Nombre"]."</td>";
                echo "<td>".$row["Apellido_P"]." ".$row["Apellido_M"]."</td>";
                echo "<td>".$row["Estado"]."</td>";
                echo "<td>".$row["Registro"]."</td>";
                echo "<td>".$row["Transicion"]." <a href='add.php?id=".$row["Id_Zombie"]."'class='right btn-flat modal-trigger' id='".$row["Id_Zombie"]."' name ='".$row["id_user"]."'><i class='  material-icons' >mode_edit</i></a></td>";
                echo "</tr>";
            }
            echo "</table>";
        }
        mysqli_free_result($result);
        cerrarBD($conexion);
       
        }
        function getZombiesEstado($Estado){
        $conexion = conectarBD();
        $sql = "SELECT * FROM Zombies WHERE Estado = '".$Estado."'";
        $result = mysqli_query($conexion,$sql);
        if(mysqli_num_rows($result) > 0){
            echo '<table class="responsive-table striped ses" ><tr><th>Nombre</th><th>Apellidos</th><th>Estado</th><th>Registro</th><th>Transicion</th></tr>';
            while($row = mysqli_fetch_assoc($result)){
                echo "<tr>";
                echo "<td>".$row["Nombre"]."</td>";
                echo "<td>".$row["Apellido_P"]." ".$row["Apellido_M"]."</td>";
                echo "<td>".$row["Estado"]."</td>";
                echo "<td>".$row["Registro"]."</td>";
                echo "<td>".$row["Transicion"]."<a href='add.php?id=".$row["id_user"]."'class='right btn-flat modal-trigger' id='".$row["id_user"]."' name ='".$row["id_user"]."'><i class='  material-icons' >mode_edit</i></a></td>";
                echo "</tr>";
            }
            echo "</table>";
        }
        mysqli_free_result($result);
        cerrarBD($conexion);
       
        }
          function getById($conexion, $id){
        //Specification of the SQL query
        $consulta='SELECT * FROM Zombies WHERE Id_Zombie = '.$id.''; // LLamada al procedimiento Lab 21
       $resultado = mysqli_query($conexion,$consulta);
        $line = mysqli_fetch_array($resultado, MYSQLI_BOTH);
        
        cerrarBD($conexion);//Ciera la bae de datos
        return $line;
        
        
    }
        function editEntry($Id_Zombie,$Estado){
    $conn = conectarBD();
    
    // insert command specification 
    $query='UPDATE Zombies SET Estado=?, Transicion =  CURRENT_TIMESTAMP() WHERE Id_Zombie=?';
    // Preparing the statement 
    if (!($statement = $conn->prepare($query)))  {
        die("Preparation failed: (" . $conn->errno . ") " . $conn->error. '      Regresa y completa los valores correctamente');
    }
    // Binding statement params 
    if (!$statement->bind_param("ss",$Estado,$Id_Zombie)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error. '      Regresa y completa los valores correctamente'); 
    }
    // update execution
    if ($statement->execute()) {
        echo 'There were ' . mysqli_affected_rows($mysql) . ' affected rows';
    } else {
        die("Update failed: (" . $statement->errno . ") " . $statement->error. '      Regresa y completa los valores correctamente');
    }
     cerrarBD($conexion);//Ciera la bae de datos
        }  
?>