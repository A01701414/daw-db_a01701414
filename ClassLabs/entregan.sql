SET DATEFORMAT dmy;

BULK INSERT equipo08.[Entregan]
   FROM 'e:\wwwroot\equipo08\A01701414\entregan.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )