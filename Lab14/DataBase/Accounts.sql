-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 06, 2017 at 10:07 PM
-- Server version: 5.5.57-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Accounts`
--
CREATE DATABASE IF NOT EXISTS `Accounts` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `Accounts`;

-- --------------------------------------------------------

--
-- Table structure for table `Personal_Info`
--

CREATE TABLE IF NOT EXISTS `Personal_Info` (
  `id_user` int(3) NOT NULL AUTO_INCREMENT,
  `Name` varchar(40) NOT NULL,
  `Lastname` varchar(40) NOT NULL,
  `S_Lastname` varchar(40) NOT NULL,
  `Email` varchar(40) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `Phone` varchar(20) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `Personal_Info`
--

INSERT INTO `Personal_Info` (`id_user`, `Name`, `Lastname`, `S_Lastname`, `Email`, `Address`, `Phone`) VALUES
(1, 'Manuel', 'Garcia', 'Huerta', 'manuelgh98@hotmail.com', 'Cerrada San Gabriel ', '4271211106'),
(3, 'Rodolfo ', 'Quiroz', 'Dominguez', 'rquiroz@gmail.com', 'Buena Vista 585', '442 139 0759'),
(4, 'Alejandro', 'Diaz', 'Ordaz', 'alex9780@yahoo.com', 'Avenida Siempreviva 415', '416 234 1980'),
(7, 'Guillermo ', 'Carsolio', 'Gonzales', 'mgc@gmail.com', 'Jurica', '327 156 8909'),
(8, 'Samantha ', 'Hernandez', 'Hernandez', 'samanthahdz@hotmail.com', 'San Juan del Rio', '427 347 1345'),
(9, 'Pepe Carlos', 'Pachecho', 'Sanchez', 'josecarlospas1@gmail.com', 'San Juanito', '427 427 7200');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
