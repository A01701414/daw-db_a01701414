<?php include('_header.html'); ?>
<div class="section light-blue white-text center-align">
    <h4 class="uppercase">Artículos</h4>
</div>

<div class="container">
    <div class="row">
        <h5>Sección 1</h5>
        <div class="col s4 m4 l4">
            <div class="card hoverable">
                <div class="card-image">
                    <img src="https://lorempixel.com/580/250/nature/1">
                    <span class="card-title">Título</span>
                </div>
                <div class="card-action">
                    <a href="#"><i class="small material-icons">delete_forever</i></a>
                    <a href="#" class="right align"><i class="small material-icons">edit</i></a>
                </div>
            </div>
        </div>
        <div class="col s4 m4 l4">
            <div class="card hoverable">
                <div class="card-image">
                    <img src="https://lorempixel.com/580/250/nature/1">
                    <span class="card-title">Título</span>
                </div>
                <div class="card-action">
                    <a href="#"><i class="small material-icons">delete_forever</i></a>
                    <a href="#" class="right align"><i class="small material-icons">edit</i></a>
                </div>
            </div>
        </div>
        <div class="col s4 m4 l4">
            <div class="card hoverable">
                <div class="card-image">
                    <img src="https://lorempixel.com/580/250/nature/1">
                    <span class="card-title">Título</span>
                </div>
                <div class="card-action">
                    <a href="#"><i class="small material-icons">delete_forever</i></a>
                    <a href="#" class="right align"><i class="small material-icons">edit</i></a>
                </div>
            </div>

        </div>
        <div class="col s12">
            <div class="section">
                <div class="divider"></div>
            </div>
        </div>

        <h5>Sección 2</h5>
        <div class="col s4 m4 l4">
            <div class="card hoverable">
                <div class="card-image">
                    <img src="https://lorempixel.com/580/250/nature/1">
                    <span class="card-title">Título</span>
                </div>
                <div class="card-action">
                    <a href="#"><i class="small material-icons">delete_forever</i></a>
                    <a href="#" class="right align"><i class="small material-icons">edit</i></a>
                </div>
            </div>
        </div>
        <div class="col s4 m4 l4">
            <div class="card hoverable">
                <div class="card-image">
                    <img src="https://lorempixel.com/580/250/nature/1">
                    <span class="card-title">Título</span>
                </div>
                <div class="card-action">
                    <a href="#"><i class="small material-icons">delete_forever</i></a>
                    <a href="#" class="right align"><i class="small material-icons">edit</i></a>
                </div>
            </div>
        </div>
        <div class="col s4 m4 l4">
            <div class="card hoverable">
                <div class="card-image">
                    <img src="https://lorempixel.com/580/250/nature/1">
                    <span class="card-title">Título</span>
                </div>
                <div class="card-action">
                    <a href="#"><i class="small material-icons">delete_forever</i></a>
                    <a href="#" class="right align"><i class="small material-icons">edit</i></a>
                </div>
            </div>

        </div>
    </div>
</div>
<?php include '_user-menu.html';?>
<?php include('_footer.html'); ?>
