<?php include('_header.html'); ?>
<div class="section light-blue white-text center-align">
    <h4 class="uppercase">Artículos</h4>
</div>

<div class="container">
    <div class="row">
        <h5>Sección 1</h5>
        <div class="col s4 m4 l4">
            <div class="card hoverable">
                <div class="card-image">
                    <img src="https://lorempixel.com/580/250/nature/1">
                    <span class="card-title">Título</span>
                </div>
                <div class="card-action">
                    <a href="#"><i class="small material-icons">delete_forever</i></a>
                    <a href="#" class="right align"><i class="small material-icons">edit</i></a>
                </div>
            </div>
        </div>
        <div class="col s4 m4 l4">
            <div class="card hoverable">
                <div class="card-image">
                    <img src="https://lorempixel.com/580/250/nature/1">
                    <span class="card-title">Título</span>
                </div>
                <div class="card-action">
                    <a href="#"><i class="small material-icons">delete_forever</i></a>
                    <a href="#" class="right align"><i class="small material-icons">edit</i></a>
                </div>
            </div>
        </div>
        <div class="col s4 m4 l4">
            <div class="card hoverable">
                <div class="card-image">
                    <img src="https://lorempixel.com/580/250/nature/1">
                    <span class="card-title">Título</span>
                </div>
                <div class="card-action">
                    <a href="#"><i class="small material-icons">delete_forever</i></a>
                    <a href="#" class="right align"><i class="small material-icons">edit</i></a>
                </div>
            </div>

        </div>
        <div class="col s12">
            <div class="section">
                <div class="divider"></div>
            </div>
        </div>

        <h5>Sección 2</h5>
        <div class="col s4 m4 l4">
            <div class="card hoverable">
                <div class="card-image">
                    <img src="https://lorempixel.com/580/250/nature/1">
                    <span class="card-title">Título</span>
                </div>
                <div class="card-action">
                    <a href="#"><i class="small material-icons">delete_forever</i></a>
                    <a href="#" class="right align"><i class="small material-icons">edit</i></a>
                </div>
            </div>
        </div>
        <div class="col s4 m4 l4">
            <div class="card hoverable">
                <div class="card-image">
                    <img src="https://lorempixel.com/580/250/nature/1">
                    <span class="card-title">Título</span>
                </div>
                <div class="card-action">
                    <a href="#"><i class="small material-icons">delete_forever</i></a>
                    <a href="#" class="right align"><i class="small material-icons">edit</i></a>
                </div>
            </div>
        </div>
        <div class="col s4 m4 l4">
            <div class="card hoverable">
                <div class="card-image">
                    <img src="https://lorempixel.com/580/250/nature/1">
                    <span class="card-title">Título</span>
                </div>
                <div class="card-action">
                    <a href="#"><i class="small material-icons">delete_forever</i></a>
                    <a href="#" class="right align"><i class="small material-icons">edit</i></a>
                </div>
            </div>

        </div>

        <!-- Activador del Modal -->
        <a class="btn-flat-large modal-trigger tooltipped right" data-position="bottom" data-delay="50" data-tooltip="Crear artículo" href="#modal1"><i class="large material-icons hoverable">add</i></a>
        <!-- Contenido del Modal -->
        <div id="modal1" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4>Crear artículo</h4>
                <div class="row">
                    <form action="" class="col s12">
                        <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="Título del artículo" id="titulo" type="text" class="validate">
                                <label for="titulo">Título</label>
                            </div>
                            <div class="col s6 align-right">
                                <i class="medium material-icons">image</i>
                                <a class="waves-effect waves-teal btn-flat">Añadir imágen</a>
                            </div>
                            <div class="row">
                                <div class="input-field col s12 m8 l8">
                                    <textarea id="descrip" class="materialize-textarea" data-length="500"></textarea>
                                    <label for="decrip">Descripción</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <input placeholder="https://www.youtube.com/..." id="url" type="text" class="validate">
                                <label for="url">URL Video</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s6">
                                <div class="file-field input-field">
                                    <div class="btn">
                                        <span>Adjuntar</span>
                                        <input type="file" multiple>
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Adjuntar uno o más archivos">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Bitácora de Modificaciones</a>
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Guardar Borrador</a>
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Publicar</a>
            </div>
        </div>
        <!-- Termina contenido del modal -->
    </div>
</div>
<?php include('_footer.html'); ?>
