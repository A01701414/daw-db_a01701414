<?php include('_header.html'); ?>
<div class="section orange center white-text uppercase">
    <h4>Empleados</h4>
</div>

<div class="container">
    <div class="row">
        <div class="col s3 m3 l3">
            <p>Nombre</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Rol</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Departamento</p>
        </div>
        <div class=" col s3 m3 l3">
            <a href="#"><i class="right small material-icons">delete_forever</i></a>
            <a href="#"><i class="right small material-icons">edit</i></a>
        </div>
    </div>
    <div class="divider"></div>
    <div class="row">
        <div class="col s3 m3 l3">
            <p>Nombre</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Rol</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Departamento</p>
        </div>
        <div class=" col s3 m3 l3">
            <a href="#"><i class="right small material-icons">delete_forever</i></a>
            <a href="#"><i class="right small material-icons">edit</i></a>
        </div>
    </div>
    <div class="divider"></div>
    <div class="row">
        <div class="col s3 m3 l3">
            <p>Nombre</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Rol</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Departamento</p>
        </div>
        <div class=" col s3 m3 l3">
            <a href="#"><i class="right small material-icons">delete_forever</i></a>
            <a href="#"><i class="right small material-icons">edit</i></a>
        </div>
    </div>
    <div class="divider"></div>
    <div class="row">
        <div class="col s3 m3 l3">
            <p>Nombre</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Rol</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Departamento</p>
        </div>
        <div class=" col s3 m3 l3">
            <a href="#"><i class="right small material-icons">delete_forever</i></a>
            <a href="#"><i class="right small material-icons">edit</i></a>
        </div>
    </div>
    <!-- Activador del Modal -->
    <a class="btn-flat-large modal-trigger tooltipped right" data-position="bottom" data-delay="50" data-tooltip="Añadir empleado" href="#modal1"><i class="medium material-icons hoverable">add</i></a>
    <!-- Contenido del Modal -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Añadir Empleado</h4>
            <div class="row">
                <form class="col s12">
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="nombre" type="text" class="validate">
                            <label for="nombre">Nombre</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="ape" type="text" class="validate">
                            <label for="ape">Apellido</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="puesto" type="text" class="validate">
                            <label for="puesto">Puesto</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="usuario" type="text" class="validate">
                            <label for="usuario">Nombre Usuario</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="pass" type="password" class="validate">
                            <label for="pass">Contraseña</label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Aceptar</a>
        </div>
    </div>
</div>
<?php include('_footer.html'); ?>
