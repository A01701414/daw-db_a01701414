<?php include('_header.html'); ?>
<div class="section orange center white-text uppercase">
    <h4>Empleados</h4>
</div>

<div class="container">
    <div class="row">
        <div class="col s3 m3 l3">
            <p>Nombre</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Rol</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Departamento</p>
        </div>
        <div class=" col s3 m3 l3">
            <a href="#"><i class="right small material-icons">delete_forever</i></a>
            <a href="#"><i class="right small material-icons">edit</i></a>
        </div>
    </div>
    <div class="divider"></div>
    <div class="row">
        <div class="col s3 m3 l3">
            <p>Nombre</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Rol</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Departamento</p>
        </div>
        <div class=" col s3 m3 l3">
            <a href="#"><i class="right small material-icons">delete_forever</i></a>
            <a href="#"><i class="right small material-icons">edit</i></a>
        </div>
    </div>
    <div class="divider"></div>
    <div class="row">
        <div class="col s3 m3 l3">
            <p>Nombre</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Rol</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Departamento</p>
        </div>
        <div class=" col s3 m3 l3">
            <a href="#"><i class="right small material-icons">delete_forever</i></a>
            <a href="#"><i class="right small material-icons">edit</i></a>
        </div>
    </div>
    <div class="divider"></div>
    <div class="row">
        <div class="col s3 m3 l3">
            <p>Nombre</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Rol</p>
        </div>
        <div class="col s3 m3 l3">
            <p>Departamento</p>
        </div>
        <div class=" col s3 m3 l3">
            <a href="#"><i class="right small material-icons">delete_forever</i></a>
            <a href="#"><i class="right small material-icons">edit</i></a>
        </div>
    </div>
</div>
<?php include '_user-menu.html';?>
<?php include('_footer.html'); ?>
