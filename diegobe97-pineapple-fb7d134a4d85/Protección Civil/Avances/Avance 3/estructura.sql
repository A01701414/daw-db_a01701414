-- Creación de Tablas ---------------------------------------------------------------------

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='Usuario')
DROP TABLE Usuario

CREATE TABLE Usuario
(
	NombreUsuario varchar(20) not null,
	Contrasena varchar(20) not null,
	IdEmpleado numeric(8) not null,
	Nombre varchar(50) not null,
	ApellidoP varchar(50) not null,
	ApellidoM varchar(50),
	Puesto varchar(50)
)
-- Relación --
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='AsignadoA')
DROP TABLE AsignadoA

CREATE TABLE AsignadoA
(
	NombreUsuario varchar(20) not null,
	IdRol numeric(8) not null
)
-------------------------------
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='Rol')
DROP TABLE Rol

CREATE TABLE Rol
(
	IdRol numeric(8) not null,
	NombreRol varchar(50),
	DescripcionRol varchar(250)
)
-- Relación --
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='CuentaCon')
DROP TABLE CuentaCon

CREATE TABLE CuentaCon
(
	IdRol numeric(8) not null,
	IdPrivilegio numeric(8) not null
)
--------------------------------
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='Privilegio')
DROP TABLE Privilegio

CREATE TABLE Privilegio
(
	IdPrivilegio numeric(8) not null,
	NombrePrivilegio varchar(50) not null,
	DescripcionPrivilegio varchar(250)
)

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='Departamento')
DROP TABLE Departamento

CREATE TABLE Departamento
(
	IdDepartamento numeric(8) not null,
	NombreDepartamento varchar(50) not null,
	DescripcionDepartamento varchar(250)
)
-- Relación --
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='TrabajaEn')
DROP TABLE TrabajaEn

CREATE TABLE TrabajaEn
(
	NombreUsuario varchar(20) not null,
	IdDepartamento numeric(8) not null,
	Fecha Datetime
)
------------------------------------
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='Publicacion')
DROP TABLE Publicacion

CREATE TABLE Publicacion
(
	IdPublicacion numeric(8) not null,
	FechaPublicacion Datetime not null,
	TituloPublicacion varchar(50) not null,
	URLVideoPublicacion varchar(100),
	DescripcionPublicacion varchar(8000) not null,
	RutaAdjuntosPublicacion varchar(300),
	RutaImagenesPublicacion varchar(300),
	FijoPublicacion numeric(1),
	FechaFinFijoPublicacion Datetime,
	FechaPublicar Datetime,
	PublicacionPublicada numeric(1)
)
-- Relación --
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='PerteneceAPublicacion')
DROP TABLE PerteneceAPublicacion

CREATE TABLE PerteneceAPublicacion
(
	IdPublicacion numeric(8) not null,
	IdTipoPublicacion numeric(8) not null
)
---------------------------------------
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='TipoDePublicacion')
DROP TABLE TipoDePublicacion

CREATE TABLE TipoDePublicacion
(
	IdTipoPublicacion numeric(8) not null,
	NombreTipoPublicacion varchar(50) not null,
	DescripcionTipoPublicacion varchar(250)
)
-- Relación --
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='CorrespondeA')
DROP TABLE CorrespondeA

CREATE TABLE CorrespondeA
(
	IdPublicacion numeric(8) not null,
	IdCategoria numeric(8) not null
)
-------------------------------------
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='Categoria')
DROP TABLE Categoria

CREATE TABLE Categoria
(
	IdCategoria numeric(8) not null,
	NombreCategoria varchar(50) not null
)

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='Subcategoria')
DROP TABLE Subcategoria

CREATE TABLE Subcategoria
(
	IdSubcategoria numeric(8) not null,
	IdCategoria numeric(8) not null,
	NombreSubcategoria varchar(50) not null,
	CorreoSubcategoria varchar(50)
)
-- Relación --
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='Bitacora')
DROP TABLE Bitacora

CREATE TABLE Bitacora
(
	IdPublicacion numeric(8) not null,
	NombreUsuario varchar(20) not null,
	IdBitacora numeric(8) not null,
	Fecha Datetime not null,
	Descripcion varchar(500)
)
-------------------------------------
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='Evento')
DROP TABLE Evento

CREATE TABLE Evento
(
	IdEvento numeric(8) not null,
	TituloEvento varchar(50) not null,
	DescripcionEvento varchar(2500),
	URLVideoEvento varchar(300),
	RutaAdjuntosEvento varchar(300),
	RutaImagenesEvento varchar(300),
	FechaPublicacionEvento Datetime not null,
	FechaInicio Datetime not null,
	FechaFin Datetime,
	EventoPublicado numeric(1)
)
-- Relación --
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='PerteneceAEvento')
DROP TABLE PerteneceAEvento

CREATE TABLE PerteneceAEvento
(
	IdEvento numeric(8) not null,
	IdTipoEvento numeric(8) not null
)
--------------------------------------
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='TipoEvento')
DROP TABLE TipoEvento

CREATE TABLE TipoEvento
(
	IdTipoEvento numeric(8) not null,
	NombreTipoEvento varchar(50) not null,
	DescripcionTipoEvento varchar(250)
)
----------------------------------------------------------------------------------------------------

-- Constraints -------------------------------------------------------------------------------------

-- Llaves Primarias de Entidades --
ALTER TABLE Usuario add constraint llaveUsuario PRIMARY KEY (NombreUsuario)
ALTER TABLE Rol add constraint llaveRol PRIMARY KEY (IdRol)
ALTER TABLE Privilegio add constraint llavePrivilegio PRIMARY KEY (IdPrivilegio)
ALTER TABLE Departamento add constraint llaveDepartamento PRIMARY KEY (IdDepartamento)
ALTER TABLE Publicacion add constraint llavePublicacion PRIMARY KEY (IdPublicacion)
ALTER TABLE TipoDePublicacion add constraint llaveTipoDePublicacion PRIMARY KEY (IdTipoPublicacion)
ALTER TABLE Categoria add constraint llaveCategoria PRIMARY KEY (IdCategoria)
ALTER TABLE Subcategoria add constraint llaveSubcategoria PRIMARY KEY (IdSubcategoria)
ALTER TABLE Bitacora add constraint llaveBitacora PRIMARY KEY (IdBitacora)
ALTER TABLE Evento add constraint llaveEvento PRIMARY KEY (IdEvento)
ALTER TABLE TipoEvento add constraint llaveTipoEvento PRIMARY KEY (IdTipoEvento)

-- Llaves foráneas --
ALTER TABLE AsignadoA add constraint cfAsignadoANombreUsuario foreign key (NombreUsuario) references Usuario(NombreUsuario)
ALTER TABLE AsignadoA add constraint cfAsignadoAIdRol foreign key (IdRol) references Rol(IdRol)

ALTER TABLE CuentaCon add constraint cfCuentaConIdRol foreign key (IdRol) references Rol(IdRol)
ALTER TABLE CuentaCon add constraint cfCuentaConIdPrivilegio foreign key (IdPrivilegio) references Privilegio(IdPrivilegio)

ALTER TABLE TrabajaEn add constraint cfTrabajaEnNombreUsuario foreign key (NombreUsuario) references Usuario(NombreUsuario)
ALTER TABLE TrabajaEn add constraint cfTrabajaEnIdDepartamento foreign key (IdDepartamento) references Departamento(IdDepartamento)

ALTER TABLE PerteneceAPublicacion add constraint cfPerteneceAPublicacionIdPublicacion foreign key (IdPublicacion) references Publicacion(IdPublicacion)
ALTER TABLE PerteneceAPublicacion add constraint cfPerteneceAPublicacionIdTipoPublicacion foreign key (IdTipoPublicacion) references TipoDePublicacion(IdTipoPublicacion)

ALTER TABLE CorrespondeA add constraint cfCorrespondeAIdPublicacion foreign key (IdPublicacion) references Publicacion(IdPublicacion)
ALTER TABLE CorrespondeA add constraint cfCorrespondeAIdCategoria foreign key (IdCategoria) references Categoria(IdCategoria)

ALTER TABLE Subcategoria add constraint cfSubcategoriaIdCategoria foreign key (IdCategoria) references Categoria(IdCategoria)

ALTER TABLE Bitacora add constraint cfBitacoraIdPublicacion foreign key (IdPublicacion) references Publicacion(IdPublicacion)
ALTER TABLE Bitacora add constraint cfBitacoraNombreUsuario foreign key (NombreUsuario) references Usuario(NombreUsuario)

ALTER TABLE PerteneceAEvento add constraint cfPerteneceAEventoIdEvento foreign key (IdEvento) references Evento(IdEvento)
ALTER TABLE PerteneceAEvento add constraint cfPerteneceAEventoIdTipoEvento foreign key (IdTipoEvento) references TipoEvento(IdTipoEvento)

-- Restricciones --

ALTER TABLE Usuario add constraint Size_Contrasena check (Contrasena >= 8)
ALTER TABLE Usuario add constraint CK_Mayus CHECK (PATINDEX('%[A-Z]%', Contrasena)>0)
ALTER TABLE Usuario add constraint CK_Minus CHECK (PATINDEX('%[a-z]%', Contrasena)>0)

--------------------------------------------------------------------------------------------------------

-- Cargar datos --
--Entidades--
SET DATEFORMAT dmy

BULK INSERT equipo04.equipo04.[Usuario]
FROM 'e:\wwwroot\equipo04\Usuario.csv'
WITH
(
	FIRSTROW=2,
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
BULK INSERT equipo04.equipo04.[Rol]
FROM 'e:\wwwroot\equipo04\Rol.csv'
WITH
(
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
BULK INSERT equipo04.equipo04.[Privilegio]
FROM 'e:\wwwroot\equipo04\Privilegio.csv'
WITH
(
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
BULK INSERT equipo04.equipo04.[Departamento]
FROM 'e:\wwwroot\equipo04\Departamento.csv'
WITH
(
	FIRSTROW=2,
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
BULK INSERT equipo04.equipo04.[Publicacion]
FROM 'e:\wwwroot\equipo04\Publicacion.csv'
WITH
(
	FIRSTROW=2,
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
BULK INSERT equipo04.equipo04.[TipoDePublicacion]
FROM 'e:\wwwroot\equipo04\TipoDePublicacion.csv'
WITH
(
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
BULK INSERT equipo04.equipo04.[Categoria]
FROM 'e:\wwwroot\equipo04\Categoria.csv'
WITH
(
	FIRSTROW=2,
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
BULK INSERT equipo04.equipo04.[Subcategoria]
FROM 'e:\wwwroot\equipo04\Subcategoria.csv'
WITH
(
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
BULK INSERT equipo04.equipo04.[Evento]
FROM 'e:\wwwroot\equipo04\Evento.csv'
WITH
(
	FIRSTROW=2,
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
BULK INSERT equipo04.equipo04.[TipoEvento]
FROM 'e:\wwwroot\equipo04\TipoEvento.csv'
WITH
(
	FIRSTROW=2,
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
BULK INSERT equipo04.equipo04.[Bitacora]
FROM 'e:\wwwroot\equipo04\Bitacora.csv'
WITH
(
	FIRSTROW=2,
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
--Relaciones--
BULK INSERT equipo04.equipo04.[AsignadoA]
FROM 'e:\wwwroot\equipo04\AsignadoA.csv'
WITH
(
	FIRSTROW=2,
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
BULK INSERT equipo04.equipo04.[CuentaCon]
FROM 'e:\wwwroot\equipo04\CuentaCon.csv'
WITH
(
	FIRSTROW=2,
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
BULK INSERT equipo04.equipo04.[TrabajaEn]
FROM 'e:\wwwroot\equipo04\TrabajaEn.csv'
WITH
(
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
BULK INSERT equipo04.equipo04.[PerteneceAPublicacion]
FROM 'e:\wwwroot\equipo04\PerteneceAPublicacion.csv'
WITH
(
	FIRSTROW=2,
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
BULK INSERT equipo04.equipo04.[CorrespondeA]
FROM 'e:\wwwroot\equipo04\CorrespondeA.csv'
WITH
(
	FIRSTROW=2,
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
BULK INSERT equipo04.equipo04.[PerteneceAEvento]
FROM 'e:\wwwroot\equipo04\PerteneceAEvento.csv'
WITH
(
	FIRSTROW=2,
	CODEPAGE='ACP',
	FIELDTERMINATOR='^',
	ROWTERMINATOR='\n'
)
--------------------------------------------------------------------------------------------